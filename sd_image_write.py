__author__ = 'naveen'

# SCRIPT : sd_image_write.py
# PURPOSE: This script will write image to multiple sdcard and sent a completed mail when finished

#!/usr/bin/python3

import os
import smtplib
from subprocess import Popen, PIPE

class bootSdCard:

    # My RPI Image Path
    Image_path = "/home/naveen/Downloads/image"
    # Here my path sdcard at part /media/
    command = 'lsblk -l | tr -s " " | grep "part /media/" | cut -d" " -f1'
    # Admin Password here
    sudo_password = 'asminaveen'

    #This method is used to find RPI image version
    def RpiImageType(self):
        os.chdir(bootSdCard.Image_path)
        arr = os.listdir()
        new_arr= [k for k in arr if '.img' in k]
        count=1
        for item in new_arr:
            print (count, '--->' ,item)
            count=count+1
        ImageVersion= int(input("Enter Image Version : "))
        print('selected RPI Image Version : ', new_arr[ImageVersion-1])
        return new_arr[ImageVersion-1]

    #This method is used to find all external SdCard
    def FindSdCard(self):
        # Here my path sdcard at part /media/
        sdcard=os.popen(bootSdCard.command).read()
        sdcard = sdcard[0:-1]
        #print (sdcard)
        sdcard_list = sdcard.split('\n')
        #print(sdcard_list, len(sdcard_list))
        return sdcard, sdcard_list

    #This mrthod is used to write SdCard in parallel
    def wirteSD(self, Path, ImageVersion, sdcardMount):
        var_count=1
        var_marks=['P0']
        for sdcard in sdcardMount.splitlines():
            cmd= 'resin local flash '+ str(Path)+ '/' + str(ImageVersion) + ' --drive /dev/' + str(sdcard[0:-1]) + ' --yes'
            cmd=cmd.split()
            print(cmd)
            var_marks.append('P'+str(var_count))
            print(var_marks)
            var_marks[var_count] = Popen(['sudo', '-S'] + cmd, stdin=PIPE, universal_newlines=True)
            var_count= var_count + 1

        #del 1st(unwanted) item is list
        del var_marks[0]

        #sudo password provided here
        for i in var_marks:
            i.communicate(bootSdCard.sudo_password + '\n')

        #Check process to complete
        completed = False
        while not completed:
            for j in var_marks:
                if j.poll() is not None:
                    completed = True
                else:
                    pass

        return True


    #This method is used to send mail
    def send_text_email(self, sd_list):
        gmail_user = 'naveen.nayan@zenatix.com'
        gmail_pwd = "nay8459___NAV"
        FROM = 'naveen.nayan@zenatix.com'
        TO = ['nayan.the.golden.eye@gmail.com' ,'nayansoex@gmail.com' ,'harshal.chaudhari@zenatix.com']
        SUBJECT = "RPI image | SdCard "
        TEXT = "Total of " + str(len(sd_list)) + " SdCard Mount at " , sd_list , " Prepaired successfully now you can exit SdCard "

        # Prepare actual message
        message = """\From: %s\nTo: %s\nSubject: %s\n\n%s""" % (FROM, ", ".join(TO), SUBJECT, TEXT)
        try:
            server = smtplib.SMTP("smtp.gmail.com", 587)
            server.ehlo()
            server.starttls()
            server.login(gmail_user, gmail_pwd)
            server.sendmail(FROM, TO, message)
            server.close()
            print ('successfully sent the mail to : ' , TO)
        except Exception as e :
            print ("failed to send mail", e)

#This is main method program starts from here
if __name__ == "__main__":
    print('This program help you to write RPI Image on Multiple SdCard' + '\n'+
          '--usages insert multiple sdcard in usb hub wait for sdcard to open, choose RPI image and give permission to write..'+ '\n')
    obj = bootSdCard()
    Selected_ImageVersion = obj.RpiImageType()
    #print(Selected_ImageVersion)
    sdcardMount, SdCard_list = obj.FindSdCard()
    msg = str(len(SdCard_list)) + ' SdCard Mount at ' , SdCard_list , "ok"
    print(msg)
    SD_return = obj.wirteSD(obj.Image_path, Selected_ImageVersion, sdcardMount)
    if SD_return:
        obj.send_text_email(SdCard_list)
